import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class Database {
	Scanner scanner=new Scanner(System.in);
	
	
	
	public static Connection process()
	{
		try
		{
		Class.forName("com.mysql.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost/Books","root","root");
		return con;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public void insertbookdetails(int bid, String bname, int bprice) 
	{
		try 
		{
			Connection con=process();
			PreparedStatement ps=con.prepareStatement("INSERT INTO bookdetail(Bid,Bname,Bprice) VALUES (?,?,?)");
			do
			{
				ps.setInt(1,bid);
				ps.setString(2,bname);
				ps.setInt(3,bprice);
				ps.executeUpdate();
				System.out.println("Do you want add few more books say yes or no:");
				String msg1=scanner.next();
				if(msg1.startsWith("no"))
				{
					break;
				}
				else
					Books.addbook();
				
			}
			while(true);
			

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}


	public void fullBookDetails() 
	{
		// TODO Auto-generated method stub
		try
		{
			Connection con=process();
			Statement stmt=con.createStatement();  
			ResultSet rs=stmt.executeQuery("select * from bookdetail");  
			while(rs.next())  
				System.out.println(rs.getInt(1)+"  "+rs.getString(2)+" "+rs.getInt(3));  
			con.close();  
		}
		catch(Exception e)
		{
			System.out.println(e);
		}  

	}


	public void particularBookDetails(int bid)
	{
		// TODO Auto-generated method stub
		try
		{
			Connection connection=process();
			Statement statement=connection.createStatement();  
			ResultSet resultset=statement.executeQuery("select * from bookdetail where Bid="+bid);  
			while(resultset.next())  
				System.out.println(resultset.getInt(1)+"  "+resultset.getString(2)+" "+resultset.getInt(3));  
			connection.close();  
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}


	public void delete(int bid)
	{
		// TODO Auto-generated method stub
		try
		{
			int bookid=bid;
			Connection connection=process();
			PreparedStatement parepare=connection.prepareStatement("delete from bookdetail where Bid="+bookid);
			parepare.executeUpdate();
			System.out.println("particular details is deleted:");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	public void Updateprice(int bprize, int bid) 
	{
		// TODO Auto-generated method stub
		try
		{
			Connection conncetion=process();
			PreparedStatement prepare=conncetion.prepareStatement("update bookdetails set Bprice='"+bprize+"'where Bid="+bid);
			prepare.executeUpdate();
			System.out.println("particular details is updated:");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	


	}

}